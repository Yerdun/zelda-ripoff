extends KinematicBody2D

export var movementSpeed = 80	# How fast the player moves in pixels per second
var direction = Vector2.ZERO	# Vector for movement direction
onready var animationState = $AnimationTree.get("parameters/playback")	# Variable used to store which animation to play

enum {	# Enumerators for different states the player can be in. Basically each variable is assigned an integer value
	MOVE,
	SWING }
var state = MOVE	# Default state is movement


func _physics_process(delta):
	match state:	# Run specific routines based on which state player is in
		MOVE: 
			_move()
		SWING:
			_swing()
	
	_checkCollisions()	# Also check for collisions


func _getDirection():	# This reads directions for movement. Is its own function because it's used in multiple states.
	# Set direction vector to whichever directional inputs are pressed
	direction.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	direction.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	if direction != Vector2.ZERO:
		direction = direction.normalized()	# Normalise diagonal direction
		animationState.travel("MovementDirections")	# Play movement animations
		# Set direction of animations
		$AnimationTree.set("parameters/IdleDirections/blend_position", direction)
		$AnimationTree.set("parameters/MovementDirections/blend_position", direction)
		$AnimationTree.set("parameters/SwingDirections/blend_position", direction)
	else:
		animationState.travel("IdleDirections")	# Play idle animations
		direction = Vector2.ZERO	# Stop the character if no inputs are pressed


func _move():	# Handles movement
	_getDirection()	# Read inputs to get direction
	move_and_slide(direction * movementSpeed)	# Move the player by movementSpeed pixels every second in velocity vector's direction
	if Input.is_action_just_pressed("use_item"):	# name is use item because I might use it for other stuff too
		state = SWING	# Switch to swing state if the use item key is pressed (Z or Space)


func _swing():	# Handes wieldable item swinging.
	animationState.travel("SwingDirections")	# switch to SwingDirections state on Animation Tree
	if Input.is_action_just_pressed("use_item"):	# Allow interrupting a swing with another swing
		_getDirection()	# Let the player switch direction if desired
		animationState.travel("SwingDirecitons")	# Restart the animation

func _swingEnd():	# This runs upon finishing the swing animation
	state = MOVE	# Switch back to move state


func _checkCollisions():	# Handles collision checking. Can add other collision states (eg. damage, fire) later.
	if get_slide_count() > 0:	# If colliding with objects
		var collidedObject = get_slide_collision(0).collider	# Store collider in variable
		if collidedObject.is_in_group("pushable"):	# If the collided object is in group pushable
			collidedObject._push(direction)	# Run the push function. All things in pushable group will have this function
