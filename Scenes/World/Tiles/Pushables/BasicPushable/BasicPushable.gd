extends KinematicBody2D
export var pushSpeed = 16	# How fast you can push the object, in pixels

func _push(playerDirection: Vector2):	# Function just for pushing
	if abs(playerDirection.x) + abs(playerDirection.y) > 1:	# When player is moving in a diagonal
		return	# Don't push
	else:
		move_and_slide(playerDirection * pushSpeed)	# Otherwise move it in player's direction at pushSpeed magnitude
	# Potential TODO: Make diagonal pushing more faithful to BOTNES
